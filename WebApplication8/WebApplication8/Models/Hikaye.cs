﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication8.Models
{
    public class Hikaye
    {
        // [DisplayName(Resources.Resources.labelForName)]
        
        public int HikayeID { get; set; }
        [Display(Name = nameof(Resources.lang.h1), ResourceType = typeof(Resources.lang))]
        public string HikayeAdi { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        [Display(Name = nameof(Resources.lang.h2), ResourceType = typeof(Resources.lang))]
        public string Hicerik { get; set; }
        [Display(Name = nameof(Resources.lang.h3), ResourceType = typeof(Resources.lang))]
        public string Uye { get; set; }
        [Display(Name = nameof(Resources.lang.h4), ResourceType = typeof(Resources.lang))]
        public string Yazar { get; set; }
        [Display(Name = nameof(Resources.lang.h5), ResourceType = typeof(Resources.lang))]
        public DateTime Tarih { get; set; }
        // Etiket eklenecek
    }
}