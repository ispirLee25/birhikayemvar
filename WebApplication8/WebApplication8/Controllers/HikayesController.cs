﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication8.Models;

namespace WebApplication8.Controllers
{
    [Authorize]
    public class HikayesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Hikayes
        public ActionResult Index()
        {
            return View(db.Hikayes.ToList());
        }

        

        // GET: Hikayes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hikaye hikaye = db.Hikayes.Find(id);
            if (hikaye == null)
            {
                return HttpNotFound();
            }
            return View(hikaye);
        }

        // GET: Hikayes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hikayes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HikayeID,HikayeAdi,Hicerik,Uye,Yazar,Tarih")] Hikaye hikaye)
        {
            if (ModelState.IsValid)
            {
                hikaye.Hicerik = "<pre>" + hikaye.Hicerik + "</pre>";
                db.Hikayes.Add(hikaye);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hikaye);
        }

        // GET: Hikayes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hikaye hikaye = db.Hikayes.Find(id);
            if (hikaye == null)
            {
                return HttpNotFound();
            }
            return View(hikaye);
        }

        // POST: Hikayes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HikayeID,HikayeAdi,Hicerik,Uye,Yazar,Tarih")] Hikaye hikaye)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hikaye).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hikaye);
        }

        // GET: Hikayes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hikaye hikaye = db.Hikayes.Find(id);
            if (hikaye == null)
            {
                return HttpNotFound();
            }
            return View(hikaye);
        }

        // POST: Hikayes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hikaye hikaye = db.Hikayes.Find(id);
            db.Hikayes.Remove(hikaye);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}