﻿using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using WebApplication8.Models;


namespace BirHikayemVar.Controllers
{
    
    
    public class BlogController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public ActionResult Home1()
        {
            return View(db.Hikayes.ToList());
        }
    }
}
